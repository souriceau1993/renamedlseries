# RenameDLSeries #

Clean and easy tool to rename downloaded series pro saison.

![step1.png](https://bitbucket.org/repo/dMkgrK/images/3221820131-step1.png)

![step2.png](https://bitbucket.org/repo/dMkgrK/images/3301235768-step2.png)

Enjoy !

[Download Here](https://bitbucket.org/souriceau1993/renamedlseries/downloads/RenameDLSeries.jar)

### Versions ###

* actual v1.0

### How do I get set up? ###

* Install Java on your machine
* Download and run ;-)

### Limit ###

* Actually support all files ;-)

### Contribution guidelines ###

* OctetCodeur

### Contact ###

gilles.savary93@gmail.com